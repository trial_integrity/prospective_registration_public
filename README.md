This page presents the data and the code associated with the article "Prospective Registration of Trials: Where we are, why, and how we could get better.", by Denis MONGIN, Diana BUITRAGO-GARCIA , Sami CAPDEROU, Thomas AGORITSAS, Cem GABAY, Delphine Sophie COURVOISIER, Michele IUDICI, published in Journal of Clinical Epidemiology
doi:  10.1016/j.jclinepi.2024.111586 

This repository is organized as follows:

- the [getting_pubmed_data](./getting_pubmed_data) folder contains the R script and the file allowing to querry the pubmed data
  - the csv file [rheumatho_journals.csv](./data/rheumatho_journals.csv) contains the list of rheumatology journals and their respective ISSNs
  - the csv file [general_journals.csv](./general_journals.csv) contains the list of general newspapers and their ISSNs
  - the csv file [keywords_general_journals.csv](./keywords_general_journals.csv) contains the list of keywords used to detect rheumatology articles in general journals
  - the R file [FosteringTransparency_get_Pubmed_data.R](./FosteringTransparency_get_Pubmed_data.R) contains the code used to perform the PubMed request and download the xml metadata used in the article.

- the [data](./data) folder contains the clean data analyzed in the article
  - the csv file [full_list.csv](./data/full_list.csv) contains the list of articles obtained through the PubMed search with their ID, title, and status in the analysis (included or exluded) and the reason 
  - the csv file [redcap_survey_notrn.csv](./data/redcap_survey_notrn.csv) contains the list of answer for the redcap form send to authors who did not register their RCT.
  - the csv file [redcap_survey_retro.csv](./data/redcap_survey_retro.csv) contains the list of answer for the redcap form send to authors who did register retrospectively their RCT.
  - the csv file [redcap_survey_datadictionnary.csv](./redcap_survey_datadictionnary.csv) contains the data dictionnary of the two redcap forms
  - the csv file [FosteringTransparency_fulldata.csv](./FosteringTransparency_fulldata.csv) contains for all articles used for analysis :
    + 'ID', the PubMed ID of the article
    + 'Title', the title of the article
    + 'Medline_date', the date the article was registered on PubMed
    + 'Year', the corresponding year
    + 'Journal_title', the name of the journal in which the article was published
    + 'ISSN', the ISSN number of the journal
    + 'ICMJE', Yes: if the journal declared that it followed ICMJE recommendations at the time of publication, No otherwise
    + 'ImpactFactor', the journal's Impact Factor at the time of publication
    + 'Year_start' trial start year for articles without TRN
    + 'Condition_long' Health Condition studied by the trial
    + 'Country' country of recruitment. International if more than one
    + 'Continent' continent of the country. Transcontinental if more than one	
    + 'International' National: if there is one country of recruitment, International: if there are at least two countries of recruitement
    + 'Sponsor_long' primary sponsor 
    + 'Sponsor' Industry: if the primary sponsor is an industry, "Non-industry': otherwise (university, government body,...)	
    + 'Intervention_long' short description of the intervention
    + 'Intervention' Drugs: if the trial involves a drug, 'Non-drug': otherwise (behaviour, device,...)
    + 'Target_sample_size' number of patients targeted by the trial
    + 'Sample_Size' categorized target sample size : "<100", "100-500", ">500"
    + 'TRN' Trial Register Number, "noTRN" if no one was find in the article
    + 'Prospective_WHO' Yes: if the RCT is declared prospective on the WHO database	
    + 'Date_registration_WHO' date of registration of the trial	obtained on the WHO database
    + 'Date_first_enrollment_WHO'	date of first enrollment of the trial	obtained on the WHO database
    + 'Difference_date' Date of registration - Date of first enrollment
    + 'Prospective' 1: if Prospective_computed = Yes, 0: if (Prospective_computed = Yes or TRN = 'noTRN')
    + 'Condition_short' condition studied by the RCT, with less conditions
    + 'Nauthor' Number of authors in the publication
    + 'source' source of the TRN: metadata, abstract, full text, or noTRN
  - the csv file [register_toverify.csv](./register_toverify.csv) contains the date extracted from WHO-ICTRP and the corresponding register for a random sample of 90 RCTs, to verify that the information provided by WHO-ICTRP was reliable and up to date.

- the [Analysis_fostering_transparency.R](./Analysis_fostering_transparency.R) file contains the analysis, producing the tables and figure of the article. The [FosteringTransparency_functions_LogR.R](FosteringTransparency_functions_LogR.R) file contains functions used in the analysis

